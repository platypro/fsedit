fsedit - A modern and intuitive GUI for managing mounts
=======================================================

![FSedit screenshot](media/screenshot.png)

Fsedit is a program for managing your system mounts through the fstab file. It differs from others in that it hides much of the busywork involved when configuring mount entries. Mount entries are built using 'presets', which define fields shown to the user, as well as how those fields correspond to a fstab entry. 

When loading a fstab file, fsedit tries it's best to predict the preset to use for each entry. If it cant find a suitable preset, it defaults to the 'custom' preset, which provides 1:1 mapping of user fields to fstab fields.

Fsedit tries to follow best practices by (optionally) using external credential files whenever possible. Generally, the credentials are stored alongside the fstab file in a directory accessible only to the user who owns the fstab, although induvidual presets may differ. Extra details on how each preset handles credential files can be found under [Available Presets](#available-presets).

While fsedit tries to keep untouched mount entries act functionally the same, errors may still happen. Feel free to open a GitLab Issue if something unexpected happens. That said, no warranty is provided for any damage caused by fsedit. See [LICENSE](LICENSE) for more info.

## Available Frontends
 * GTK Frontend
 * Test (Demo) Frontend
More frontends may come in the future... 

## Available Presets
### Local Device Preset
Fsedit can edit mount entries for local block devices. Three presets are provided:
 * CD/DVD
 * Floppy
 * Generic Block device

### CIFS
Fsedit supports CIFS mounts with or without external credential files. Credential files are stored alongside the fstab file and are only accesible to the fstab owner.

### NFS
Fsedit supports simple NFS mounts.

# Building
**Warning: Meson version 0.44.0 will not work for building the GTK frontend, there is a bug involving resource files which generates invalid paths**

Fsedit is built using meson and requires version 0.37.0 or later. To compile, create a new directory for your build then run meson, with your new build directory as its argument. Change directories to your build dir then run your build command, which for meson is usually "ninja". To install run "sudo ninja install". 

Different frontends are installed into (Build Dir)/src/frontend.(type)

If running the GTK frontend without installation, make sure to set the environment veriable "GSETTINGS_SCHEMA_DIR" to be the "resource" folder within the gtk frontend build directory.

# Helping out
Bug reports and merge requests are welcome.

# Ideas for future
 * ftpfs preset
 * sshfs preset
 * manpage viewer
 * device browser