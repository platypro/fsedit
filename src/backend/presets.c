//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "General.h"
#include "backend.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <blkid.h>
#include <assert.h>

#include <modules/gb_string.h>

//CIFS
bool cifs_toOpts(PRESETDATA* pdata)
{
	ADDENTRY* from = pdata->entry;
	MOUNT_STRUCT* to = pdata->mount;
	if(from->perms & ADDENTRY_PERMS_NOLOGIN)
	{
		preset_option_push(to, "guest", NULL);
	}
	else
	{
		if(pdata->opts->useCredentials)
		{
			FILE* f = NULL;
			char* credfile = preset_file_load(pdata, "credentials", &f);
			if(f)
			{
				fprintf(f, "username=%s\npassword=%s\n",from->ruser, from->rpass);
				fclose(f);
			}
			
			preset_option_push(to, "credentials", credfile);
		}
		else
		{
			preset_option_push(to, "username", from->ruser);
			preset_option_push(to, "password", from->rpass);
		}
	}
	
	if(from->perms & ADDENTRY_PERMS_ALLUSERS)
	{
		preset_option_push(to, "uid", "root");
		preset_option_push(to, "gid", "root");
	} 
	else
	{
		preset_option_push(to, "uid", from->luser);
		preset_option_push(to, "gid", from->lgroup);
	}
	
	char perms[4];
	preset_perms_gen(from, perms);
	
	preset_option_push(to, "file_mode", perms);
	preset_option_push(to, "dir_mode", perms);
	preset_option_push(to, from->opts, NULL);
	return true;
}

bool creds_getOpts(char* key, char* value, PRESETDATA* pdata)
{
	OP_CASE_BEGIN("username")
		OP_COPY(pdata->entry->ruser)
	OP_CASE("password")
		OP_COPY(pdata->entry->rpass)
	OP_CASE_END
}

bool cifs_getOpts(char* key, char* value, PRESETDATA* pdata)
{
	OP_CASE_BEGIN("guest")
		pdata->entry->perms |= ADDENTRY_PERMS_NOLOGIN;
	OP_CASE("credentials")
	{
		char* contents = preset_file_eat(pdata, value, "credentials");
		if(contents)
		{
			preset_option_parse(pdata, creds_getOpts, contents);
			free(contents);
		}
	}
	OP_CASE("username")
		OP_COPY(pdata->entry->ruser)
	OP_CASE("password")
		OP_COPY(pdata->entry->rpass)
	OP_CASE("uid")
		OP_COPY(pdata->entry->luser)
	OP_CASE("gid")
		OP_COPY(pdata->entry->lgroup)
	OP_CASE("file_mode")
		pdata->entry->perms |= preset_perms_parse(value);
	OP_CASE("dir_mode")
		pdata->entry->perms |= preset_perms_parse(value);
	OP_CASE_END
}

bool cifs_toEntry(PRESETDATA* pdata)
{
	preset_option_parse(pdata, cifs_getOpts, MOUNT_OPTS(pdata->mount));
	return true;
}

bool local_toOpts(PRESETDATA* pdata)
{
	size_t devSize;
	const char* uuid = NULL;
	blkid_probe id_probe = blkid_new_probe_from_filename(pdata->entry->device);
	if(!id_probe) return false;
	blkid_do_probe(id_probe);
	blkid_probe_lookup_value(id_probe, "UUID", &uuid, NULL);
	
	devSize = 6 + strlen(uuid);
	MOUNT_DEVICE(pdata->mount) = malloc(devSize);
	assert(MOUNT_DEVICE(pdata->mount));
	snprintf(MOUNT_DEVICE(pdata->mount), devSize, "UUID=%s", uuid);
	return true;
}

bool local_toEntry(PRESETDATA* pdata)
{
	if(strcmp("UUID=", MOUNT_DEVICE(pdata->mount)) < 0)
	{
		pdata->entry->device = strdup(blkid_evaluate_tag(MOUNT_DEVICE(pdata->mount), NULL, NULL));
	} else return false;
	
	return true;
}

const char* cifs_type[] = {"cifs", NULL};
const char* nfs_type[] = {"nfs", NULL};
const char* local_type[] = {"auto","ext4","ext3","ext2","fat","exfat", NULL};
const char* auto_type[] = {"auto", NULL};
const char* cd_type[] = {"iso9660","auto", NULL};

MOUNTPRESET fsedit_presets[NUMPRESETS] =
{
	{
		"Internet (CIFS)", 
		cifs_type, 
		"_netdev",
		MFIELD_LOCALPERMS | MFIELD_AUTH | PASSTHROUGH_TYPE | PASSTHROUGH_DEVICE | PASSTHROUGH_FILE,
		2, "/", "//server/share",
		NULL,
		cifs_toOpts, cifs_toEntry
	},
	{
		"Internet (NFS)", 
		nfs_type,
		"_netdev,intr,hard,nfsvers=3,tcp,timeo=600",
		MFIELD_FORCEADVANCED,
		0, ":/", "server:/share",
		NULL,
		NULL, NULL
	},
	{
		"Local Device (Block)", 
		local_type,
		"defaults",
		MFIELD_LOCALPERMS | PASSTHROUGH_TYPE | PASSTHROUGH_OPTS | PASSTHROUGH_FILE,
		5, "/", "/dev/sdXX",
		"/dev/sd",
		local_toOpts, local_toEntry
	},
	{
		"Local Device (Floppy)",
		auto_type,
		"rw,noauto,user",
		MFIELD_LOCALPERMS,
		5, "/", "/dev/fdX",
		"/dev/fd",
		NULL, NULL
	},
	{
		"Local Device (CD/DVD)",
		cd_type,
		"ro,noauto,user",
		MFIELD_LOCALPERMS,
		5, "/", "/dev/cdrom",
		"/dev/cdrom",
		NULL, NULL
	},
	{
		"Custom", 
		auto_type, 
		"defaults",
		MFIELD_FORCEADVANCED,
		0,"","",
		NULL,
		NULL, NULL
	}
};
