//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "General.h"
#include "backend.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <grp.h>
#include <pwd.h>

#define GB_STRING_IMPLEMENTATION
#include <modules/gb_string.h>

uint8_t getPreset(MOUNTPRESET* presets, uint8_t numpresets, MOUNT_STRUCT* mount)
{
	int i;
	char* hint;
	
	for(i = 0; i < numpresets; i++)
	{
		hint = (presets + i)->parsehint;
		if(hint && strcmp(hint, MOUNT_DEVICE(mount)) < 0)
		{
			return i;
		}
	}
	
	for(i = (numpresets-1); i >= 0; i--)
	{
		const char** type = (presets + i)->type;
		while(*type)
		{
			if(!strcmp(*type, MOUNT_TYPE(mount)))
			{
				return i;
			}
			type++;
		}
	}
	
	return numpresets - 1;
}

int mkdir_p(char* file_path, mode_t mode, uid_t usr, uid_t grp) {
  char* p;
  for (p=strchr(file_path+1, '/'); p; p=strchr(p+1, '/')) {
    *p='\0';
    if (mkdir(file_path, mode)==-1) {
      if (errno!=EEXIST) { *p='/'; return -1; }
    }
	else
	{
		chown(file_path, usr, grp);
	}
    *p='/';
  }
  return 0;
}

bool updatePrivate(char** privateLocation, char* path)
{
	//update private path
	char* newLoc = *privateLocation;
	if(**privateLocation != '/')
	{
		//Append extension
		size_t newPathSize = 2 + strlen(path) + strlen(*privateLocation);
		newLoc = malloc(newPathSize);
		if(newLoc) 
			snprintf(newLoc, newPathSize, "%s.%s", path, *privateLocation);
	}
	return (*privateLocation = newLoc) != NULL;
}

bool mount_load (MOUNTDEF** ref, char* path, IO_OPTS* opts)
{
	MOUNT_STRUCT* mnt;
	MOUNTDEF* lastdef;
	
	mount_clear(ref);
	
	MOUNT_OPEN_FILE(tabfile, path);

	if(tabfile && updatePrivate(&opts->privatePath, path))
	{
		PRESETDATA data;
		data.opts = opts;
		data.fail = false;
		while((mnt = MOUNT_READ_ENTRY(tabfile)))
		{
			MOUNTDEF* newdef = calloc(1, sizeof(MOUNTDEF));
	
			newdef->preset = getPreset(fsedit_presets, NUMPRESETS, mnt);
			MOUNTPRESET cpreset = fsedit_presets[newdef->preset];
			
			//Get identity info from path
			struct stat filestat = {0};
			stat(MOUNT_FILE(mnt), &filestat);
			newdef->entry.luser  = strdup(getpwuid(filestat.st_uid)->pw_name);
			newdef->entry.lgroup = strdup(getgrgid(filestat.st_gid)->gr_name);
			
			bool overrPT = true;
			
			if (cpreset.toEntry)
			{
				data.entry = &newdef->entry;
				data.paths = &newdef->paths;
				data.mount = mnt;
				overrPT = !cpreset.toEntry(&data);
			}
				
			//Pass-through
			if(overrPT || cpreset.fields & PASSTHROUGH_OPTS)
				newdef->entry.opts = strdup(MOUNT_OPTS(mnt));
			if(overrPT || cpreset.fields & PASSTHROUGH_DEVICE)	
				newdef->entry.device = strdup(MOUNT_DEVICE(mnt));
			if(overrPT || cpreset.fields & PASSTHROUGH_TYPE)
				newdef->entry.type = strdup(MOUNT_TYPE(mnt));
			if(overrPT || cpreset.fields & PASSTHROUGH_FILE)
			{
				//Remove mount prefix (if there is one)
				char* oldfn = MOUNT_FILE(mnt);
				if(oldfn == strstr(oldfn, opts->mountPath))
				{
					oldfn += strlen(opts->mountPath);
				}
				newdef->entry.file = strdup(oldfn);
			}
				
			newdef->entry.pass = MOUNT_PASS(mnt);
			newdef->entry.dump = MOUNT_DUMP(mnt);
				
			//Append to the list
			if(!*ref) (*ref) = newdef;
			else      lastdef->next = newdef;
			lastdef = newdef;
		}
		MOUNT_CLOSE_FILE(tabfile);
		return data.fail;
	} 
	else if(errno == EACCES)
	{ 
		opts->error(opts->state ,MOUNT_ERR_PERMS, "fsedit", path); 
		return 1;
	}
	return 0;
}

bool mount_save (MOUNTDEF** ref, char* path, IO_OPTS* opts)
{
	MOUNTDEF* mounts = *ref;
	FILE* f = fopen(path, "w");
	if(f && updatePrivate(&opts->privatePath, path))
	{
		PRESETDATA data;
		char newName[FILENAME_MAX];
		data.fail = false;
		data.opts = opts;
		//Collect fallback ownership info (match fstab)
		struct stat fstabstat;
		stat(path, &fstabstat);
		data.stat = &fstabstat;
		
		fputs(HEADER_MESSAGE, f);
		while(mounts)
		{
			MOUNT_STRUCT fs = {0};
			MOUNTPRESET cpreset = fsedit_presets[mounts->preset];
			bool overrPT = true;
			
			data.entry = &mounts->entry;
			if(cpreset.toOpts)
			{
				data.paths = &mounts->paths;
				data.mount = &fs;
				if(!(cpreset.fields & PASSTHROUGH_OPTS))
					MOUNT_OPTS(&fs) = gb_make_string("");
				overrPT = !cpreset.toOpts(&data);
			}
			
			#define FLAGCHECK(flag) (overrPT || cpreset.fields & (flag))
			
			int nameEnd = snprintf(newName, FILENAME_MAX, "%s%s/", 
				ISNOTPATH(mounts->entry.file)
				 ? opts->mountPath
				 : "",
				FLAGCHECK(PASSTHROUGH_FILE) 
				 ? mounts->entry.file
				 : MOUNT_FILE(&fs) );
			
			mode_t dirmode = preset_perms_gen(&mounts->entry, NULL);
			uid_t mountUid;
			gid_t mountGid;
			if(data.entry->luser)
			{
				struct passwd* pd = getpwnam(data.entry->luser);
				if(pd) mountUid = pd->pw_uid;
				else   opts->error(opts->state, MOUNT_ERR_BADUSER, data.entry->device, data.entry->luser);
			} else mountUid = data.stat->st_uid;
			
			if(data.entry->lgroup)
			{
				struct group* pd = getgrnam(data.entry->lgroup);
				if(pd) mountGid = pd->gr_gid;
				else   opts->error(opts->state, MOUNT_ERR_BADGROUP, data.entry->device, data.entry->lgroup);
			} else mountGid = data.stat->st_gid;
			
			mkdir_p(
				newName, dirmode, 
				mountUid, 
				mountGid);
			
			*(newName + nameEnd - 1) = '\0';
			
			fprintf(
				f,
				"%s\t%s\t%s\t%s\t%d\t%d\n",
				FLAGCHECK(PASSTHROUGH_DEVICE) 
				 ? mounts->entry.device
				 : MOUNT_DEVICE(&fs),
				newName, 
				FLAGCHECK(PASSTHROUGH_TYPE) 
				 ? mounts->entry.type
				 : MOUNT_TYPE(&fs) , 
				FLAGCHECK(PASSTHROUGH_OPTS) 
				 ? mounts->entry.opts
				 : MOUNT_OPTS(&fs) , 
				mounts->entry.dump  , 
				mounts->entry.pass  );
				
			free(MOUNT_DEVICE(&fs));
			free(MOUNT_FILE(&fs));
			free(MOUNT_TYPE(&fs));
			if(cpreset.fields & PASSTHROUGH_OPTS)
				free(MOUNT_OPTS(&fs));
			else
				gb_free_string(MOUNT_OPTS(&fs));

			mounts = mounts->next;
		}
		fclose(f);
		free(opts->privatePath);
		return data.fail;
	} 
	else if(errno == EACCES)
	{
		opts->error(opts->state, MOUNT_ERR_PERMS, "fsedit", path);
		return 1;
	}
	return 0;
}

bool mount_add   (MOUNTDEF** ref, MOUNTDEF** add)
{
	MOUNTDEF* head = *ref;
	MOUNTDEF* newName = calloc(1, sizeof(MOUNTDEF));
	if(newName)
	{
		while(head && head->next)
			head = head->next;
		
		if(head) head->next = newName;
		else           *ref = newName;
	}
	
	*add = newName;
	return 0;
}

bool mount_rm (MOUNTDEF** head, MOUNTDEF* def)
{
	MOUNTDEF *rem, *iter = *head;
	if(iter == def)
	{
		rem = iter;
		*head = iter->next;
	}
	else
	{	
		while(iter->next && iter->next != def)
		{
			iter=iter->next;
		}
		
		rem = iter->next;
	}
	
	if(rem)
	{
		iter->next = rem->next;
		free(rem);
	}        
	return 0;
}

//bool mount_clear(MOUNTDEF** head)
bool mount_clear (MOUNTDEF** head)
{
	while(head && *head)
		mount_rm(head, *head);
	return 0;
}

//Frontend helpers

bool mount_preset_predict(PRESET_REF preset, const char* fsname, char* buffer)
{
	MOUNTPRESET* type = &fsedit_presets[preset];
	//Cut beginning
	strncpy(buffer, 
		!strncmp(fsname, type->hint, type->autoname_cut) 
			? fsname + type->autoname_cut
			: fsname, 
		MOUNT_FILE_MAX - 1
	);
	
	if(buffer)
	{
		char* c = buffer;
		while(*c)
		{
			if(strchr(type->autoname_seperate, *c))
				*c = AUTONAME_SEPERATOR;
			c++;
		}
	}
	
	return 0;
}

//Preset helpers

char* pushOption(char* newOpts, char* keymark)
{
    gbUsize curr_len = gb_string_length(newOpts);
    int other_len = strlen(keymark);
    newOpts = gb_string_make_space_for(newOpts, other_len + 1);

    newOpts[curr_len] = ',';
    memcpy(newOpts + curr_len + 1, keymark, other_len);
    newOpts[curr_len + other_len + 1] = '\0';
    gb_set_string_length(newOpts, curr_len + other_len + 1);
    
    return newOpts;
}

#define preset_iterOpts_domap \
	if(!map(keymark, valuemark, pdata)) \
	{if(valuemark) *(valuemark - 1) = '='; newOpts = newOpts ? pushOption(newOpts, keymark) : gb_make_string(keymark);}

bool preset_option_parse(PRESETDATA* pdata, OPT_MAP map, char* opts)
{
	char* butcher, *keymark, *valuemark = NULL;
	char* newOpts = NULL;
	butcher = strdup(opts);
	opts = butcher;
	keymark = butcher;
	
	while(*opts)
	{
		char* peek = opts + 1;
		switch(*opts)
		{
			case '=':
			case ' ':
				*opts = '\0';
				valuemark = *peek != ',' ? peek : NULL;
				break;
			case ',':
			case '\n':
				*opts = '\0';
				preset_iterOpts_domap;
				
				keymark   = *peek != '=' ? peek : NULL;
				valuemark = NULL;
				break;
		}
		
		opts++;
	}
	
	if(keymark) preset_iterOpts_domap;
	
	pdata->entry->opts = newOpts ? strdup(newOpts) : "";
	
	free(butcher);
	return true;
}

void preset_file_add(PATHLIST** paths, char* file, const char* extension)
{
	//Append this to path list
	PATHLIST* newpath = malloc(sizeof(PATHLIST));
	assert(newpath);
	
	newpath->extension = extension;
	newpath->path      = strdup(file);
	
	newpath->next = *paths;
	*paths = newpath;
}

char* preset_file_load(PRESETDATA* pdata, const char* extension, FILE** file)
{
	char* result = NULL; 
	PATHLIST* pathat = *pdata->paths;
	while(pathat)
	{
		if(!strcmp(extension, pathat->extension))
			result = pathat->path;
		pathat = pathat->next;
	}
	if(!result)
	{
		size_t resultLen = 
			strlen(pdata->opts->privatePath) + 
			strlen(pdata->entry->file) + 
			strlen(extension) + 4;
		if(ISNOTPATH(pdata->entry->file))
			resultLen += strlen(pdata->opts->mountPath);
			
		result = malloc(resultLen);
		assert(result);
		snprintf(
			result, resultLen, "%s%s%s.%s", 
			pdata->opts->privatePath, 
			ISNOTPATH(pdata->entry->file) 
			? pdata->opts->mountPath : "", 
			pdata->entry->file, extension);
		mkdir_p(result, S_IRWXU, pdata->stat->st_uid, pdata->stat->st_gid);
		preset_file_add(pdata->paths, result, extension);
	}
	
	if(file)
	{
		int fd = open(result, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
		if(fd == -1)
		{
			pdata->opts->error(pdata->opts->state, MOUNT_ERR_PERMS, pdata->entry->device, result);
			pdata->fail = true;
		}
		else
		{
			fchown(fd, pdata->stat->st_uid, pdata->stat->st_gid);
			*file = fdopen(fd, "w");
		}
	}

	return result;
}

PRIVATE char* eatfile(char* filename)
{
	char* result = NULL;
	//Load the file
	FILE* filestream = fopen(filename,"r");

	if (filestream)
	{
		int filesize;

		//Get the file size
		fseek(filestream, 0, SEEK_END);
		filesize = ftell(filestream);
		fseek(filestream, 0, SEEK_SET);

		//Allocate room for file
		result = calloc(1, filesize+1);
		if (result)
		{
			//Eat (Load) the file
			fread(result, sizeof(char), filesize, filestream);
		}
		fclose(filestream);
	}

	return result;
}

char* preset_file_eat(PRESETDATA* pdata, char* fileOpen, const char* extension)
{
	char* result = eatfile(fileOpen);
	if(result) preset_file_add(pdata->paths, fileOpen, extension);
	else 
	{
		pdata->opts->error(pdata->opts->state, MOUNT_ERR_PERMS, MOUNT_DEVICE(pdata->mount), fileOpen);
		pdata->fail = true;
	}
	return result;
}

mode_t preset_perms_gen(ADDENTRY* from, char* buffer)
{
	mode_t result = S_IRUSR | S_IXUSR | S_IRGRP | S_IXGRP;

	if(from->perms & ADDENTRY_PERMS_ALLUSERS)
	{
		if(from->perms & ADDENTRY_PERMS_WRITE)
		 { result = S_IRWXO | S_IRWXG | S_IRWXU; }
		else
		 { result = S_IRUSR | S_IROTH | S_IRGRP | S_IXUSR | S_IXOTH | S_IXGRP; }
	}
	else 
	{
		if(from->perms & ADDENTRY_PERMS_READ)
			result |= S_IROTH | S_IXOTH;
	
		if(from->perms & ADDENTRY_PERMS_WRITE)
			result += S_IWUSR | S_IWGRP;
	}
	
	if(buffer)
		sprintf(buffer, "%04o", result);
	
	return result;
}

uint16_t preset_perms_parse(char* perms)
{
	uint16_t result = 0;
	if(!strcmp(perms, "0777")) 
		return ADDENTRY_PERMS_WRITE | ADDENTRY_PERMS_ALLUSERS;
	if(!strcmp(perms, "0555")) 
		return ADDENTRY_PERMS_ALLUSERS;
	
	if((perms[3] & 5) == 5) 
		result |= ADDENTRY_PERMS_READ;
	if((perms[1] & perms[2] & 2) == 2) 
		result |= ADDENTRY_PERMS_WRITE;

	return result;
}

bool preset_option_push(MOUNT_STRUCT* to, char* id, char* val)
{
#define APPEND_OPTION(appendee) MOUNT_OPTS(to) = gb_append_cstring(MOUNT_OPTS(to), (appendee));
	if(*MOUNT_OPTS(to) && *id)
		APPEND_OPTION(",")
	APPEND_OPTION(id)
	if(val)
	{
		APPEND_OPTION("=")
		APPEND_OPTION(val)
	}
	return true;
}
