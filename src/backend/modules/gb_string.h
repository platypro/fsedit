/* String functions. Thanks to gingerBill for this code
 * SOURCE: https://github.com/gingerBill/gb/blob/master/gb_string.h
 * MODIFICATIONS: 
 *  uses system realloc
 *  removed docs
 *  removed c++ stuff
 *  realloc allocates more space than needed
 */

#ifndef GB_STRING_INCLUDE_GB_STRING_H
#define GB_STRING_INCLUDE_GB_STRING_H

#ifndef GB_ALLOC
#define GB_ALLOC(sz) malloc(sz)
#define GB_FREE(ptr) free(ptr)
#define GB_REALLOC(ptr, sz) realloc(ptr, sz)
#endif

#include <string.h> /* Needed for memcpy and cstring functions */

typedef char *gbString;

typedef int gbBool;
#if !defined(GB_TRUE) || !defined(GB_FALSE)
#define GB_TRUE  1
#define GB_FALSE 0
#endif

#ifndef GB_SIZE_TYPE
#define GB_SIZE_TYPE
typedef size_t gbUsize;
#endif

#ifndef GB_NULLPTR
	#define GB_NULLPTR (void*)0
#endif

typedef struct gbStringHeader {
	gbUsize len;
	gbUsize cap;
} gbStringHeader;

#define GB_STRING_HEADER(s) ((gbStringHeader *)s - 1)

gbString gb_make_string(char const *str);
gbString gb_make_string_length(void const *str, gbUsize len);
void gb_free_string(gbString str);

gbString gb_duplicate_string(gbString const str);

gbUsize gb_string_length(gbString const str);
gbUsize gb_string_capacity(gbString const str);
gbUsize gb_string_available_space(gbString const str);

void gb_clear_string(gbString str);

gbString gb_append_string_length(gbString str, void const *other, gbUsize len);
gbString gb_append_string(gbString str, gbString const other);
gbString gb_append_cstring(gbString str, char const *other);

gbString gb_set_string(gbString str, char const *cstr);

gbString gb_string_make_space_for(gbString str, gbUsize add_len);
gbUsize gb_string_allocation_size(gbString const str);

gbBool gb_strings_are_equal(gbString const lhs, gbString const rhs);

gbString gb_trim_string(gbString str, char const *cut_set);

#endif /* GB_STRING_H */
#ifdef GB_STRING_IMPLEMENTATION
static void gb_set_string_length(gbString str, gbUsize len) {
	GB_STRING_HEADER(str)->len = len;
}

static void gb_set_string_capacity(gbString str, gbUsize cap) {
	GB_STRING_HEADER(str)->cap = cap;
}


gbString gb_make_string_length(void const *init_str, gbUsize len) {
	gbString str;
	gbStringHeader *header;
	gbUsize header_size = sizeof(gbStringHeader);
	void *ptr = GB_ALLOC(header_size + len + 1);
	if (ptr == GB_NULLPTR)
		return GB_NULLPTR;
	if (!init_str)
		memset(ptr, 0, header_size + len + 1);

	str = (char *)ptr + header_size;
	header = GB_STRING_HEADER(str);
	header->len = len;
	header->cap = len;
	if (len && init_str)
		memcpy(str, init_str, len);
	str[len] = '\0';

	return str;
}

gbString gb_make_string(char const *str) {
	gbUsize len = str ? strlen(str) : 0;
	return gb_make_string_length(str, len);
}

void gb_free_string(gbString str) {
	if (str == GB_NULLPTR)
		return;

	GB_FREE((gbStringHeader *)str - 1);
}

gbString gb_duplicate_string(gbString const str) {
	return gb_make_string_length(str, gb_string_length(str));
}

gbUsize gb_string_length(gbString const str) {
	return GB_STRING_HEADER(str)->len;
}

gbUsize gb_string_capacity(gbString const str) {
	return GB_STRING_HEADER(str)->cap;
}

gbUsize gb_string_available_space(gbString const str) {
	gbStringHeader *h = GB_STRING_HEADER(str);
	if (h->cap > h->len)
		return h->cap - h->len;
	return 0;
}

void gb_clear_string(gbString str) {
	gb_set_string_length(str, 0);
	str[0] = '\0';
}

gbString gb_append_string_length(gbString str, void const *other, gbUsize other_len) {
	gbUsize curr_len = gb_string_length(str);

	str = gb_string_make_space_for(str, other_len);
	if (str == GB_NULLPTR)
		return GB_NULLPTR;

	memcpy(str + curr_len, other, other_len);
	str[curr_len + other_len] = '\0';
	gb_set_string_length(str, curr_len + other_len);

	return str;
}

gbString gb_append_string(gbString str, gbString const other) {
	return gb_append_string_length(str, other, gb_string_length(other));
}

gbString gb_append_cstring(gbString str, char const *other) {
	return gb_append_string_length(str, other, strlen(other));
}

gbString gb_set_string(gbString str, char const *cstr) {
	gbUsize len = strlen(cstr);
	if (gb_string_capacity(str) < len) {
		str = gb_string_make_space_for(str, len - gb_string_length(str));
		if (str == GB_NULLPTR)
			return GB_NULLPTR;
	}

	memcpy(str, cstr, len);
	str[len] = '\0';
	gb_set_string_length(str, len);

	return str;
}

gbString gb_string_make_space_for(gbString str, gbUsize add_len) {
	gbUsize len = gb_string_length(str);
	gbUsize new_len = len + (2*add_len);
	void *ptr, *new_ptr;
	gbUsize available, new_size;

	available = gb_string_available_space(str);
	if (available >= add_len) /* Return if there is enough space left */
		return str;


	ptr = (char *)str - sizeof(gbStringHeader);
	new_size = sizeof(gbStringHeader) + new_len + 1;

	new_ptr = GB_REALLOC(ptr, new_size);
	if (new_ptr == GB_NULLPTR)
		return GB_NULLPTR;
	str = (char *)new_ptr + sizeof(gbStringHeader);

	gb_set_string_capacity(str, new_len);

	return str;
}

gbUsize gb_string_allocation_size(gbString const s) {
	gbUsize cap = gb_string_capacity(s);
	return sizeof(gbStringHeader) + cap;
}

gbBool gb_strings_are_equal(gbString const lhs, gbString const rhs) {
	gbUsize lhs_len, rhs_len, i;
	lhs_len = gb_string_length(lhs);
	rhs_len = gb_string_length(rhs);
	if (lhs_len != rhs_len)
		return GB_FALSE;

	for (i = 0; i < lhs_len; i++) {
		if (lhs[i] != rhs[i])
			return GB_FALSE;
	}

	return GB_TRUE;
}

gbString gb_trim_string(gbString str, char const *cut_set) {
	char *start, *end, *start_pos, *end_pos;
	gbUsize len;

	start_pos = start = str;
	end_pos   = end   = str + gb_string_length(str) - 1;

	while (start_pos <= end && strchr(cut_set, *start_pos))
		start_pos++;
	while (end_pos > start_pos && strchr(cut_set, *end_pos))
		end_pos--;

	len = (start_pos > end_pos) ? 0 : ((end_pos - start_pos)+1);

	if (str != start_pos)
		memmove(str, start_pos, len);
	str[len] = '\0';

	gb_set_string_length(str, len);

	return str;
}



#endif /* GB_STRING_IMPLEMENTATION */

