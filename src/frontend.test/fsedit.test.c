#include <General.h>

#include <unistd.h>

#include <backend.h>

IO_OPTS opts = {
	.useCredentials    = 1,
	.privatePath       = "secure",
	.mountPath         = "/media/"
};

int main()
{
	ADDENTRY newEntry =
	{
		.perms  = ADDENTRY_PERMS_WRITE | ADDENTRY_PERMS_READ | ADDENTRY_PERMS_ALLUSERS | ADDENTRY_PERMS_NOLOGIN,
		
		.device = "//server/share",
		.file   = "server-share",
		.type   = "cifs",
		.opts   = "defaults",
		.dump   = 0,
		.pass   = 2,
		
		.luser  = getlogin(),
		.lgroup = getlogin(),
		
		.ruser  = getlogin(),
		.rpass  = "",
	};
		
	//Initialize list
	MOUNTDEF* defs = NULL;
	//Add a new mount
	MOUNTDEF* newdef;
	mount_add(&defs, &newdef);
	newdef->entry = newEntry;
	newdef->preset = 0;
	//Save the list to a local file
	mount_save(&defs, "newfiletest", &opts);
	//Clean up
	mount_clear(&defs);
}
