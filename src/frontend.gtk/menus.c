//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "General.h"
#include "config.h"
#include "menus.h"

#include "fsedit.gtk.h"
#include "backend.h"
#include "dfsedit.h"
#include "dadd.h"

#include <gtk/gtk.h>
#include <glib/gi18n.h>

MOUNTDEF* getDefinitionFromSelected(FSEDIT* s)
{
	GtkTreeIter iter;
	GtkTreeView* tv = GTK_TREE_VIEW(GETO("dfsedit_mounts"));
	GtkTreeModel* model = gtk_tree_view_get_model(tv);
	GtkTreeSelection* select = gtk_tree_view_get_selection(tv);
	gtk_tree_selection_get_selected(select, &model, &iter);
	if(iter.stamp)
	{
		MOUNTDEF* result;
		gtk_tree_model_get(model, &iter, 4, &result, -1);
		return result;
	}
	else return NULL;
}

bool do_save(FSEDIT* s, char* path, char* failError)
{
	bool result;
	UPDATE_OPTS(s);
	dfsedit_clearLog(s);
	dfsedit_showLog(s, false);
	if((result = mount_save(&s->fsentries, path, &s->opts)))
	{
		dfsedit_showLog(s, true);
	}
	return !result;
}

static void
settings_activated 
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = (FSEDIT*)state;
	GObject* settings = GETO("dsettings");
	gtk_dialog_run(GTK_DIALOG(settings));
	gtk_widget_hide(GTK_WIDGET(settings));
}

static void
about_activated 
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = (FSEDIT*)state;
	char* authors[] = {"Aeden McClain", NULL};
	
	gtk_show_about_dialog
	(
		GTK_WINDOW(GETO("dfsedit")),
		"authors", authors,
		"copyright", _("Copyright (c) 2017 Aeden McClain"),
		"license-type", GTK_LICENSE_GPL_3_0,
		"logo", gdk_pixbuf_new_from_file(APP_ICON_PATH, NULL),
		"program-name", "fsedit",
		"version", _("Version 1.0"),
		"website", "https://www.platypro.net",
		"title", _("About fsedit"),
		NULL
	);
}

static void
quit_activated
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       s)
{
  g_application_quit (G_APPLICATION (((FSEDIT*) s)->app));
}

static GActionEntry fsedit_app_actions[] =
{
	{ "settings", settings_activated, NULL, NULL, NULL },
	{ "about", about_activated, NULL, NULL, NULL },
	{ "quit", quit_activated, NULL, NULL, NULL }
};

static void
new_activated
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = (FSEDIT*)state;
	g_object_unref(s->currentFile);
	s->currentFile = NULL;
	dfsedit_updatetitle(s);
	mount_clear(&s->fsentries);
	dfsedit_showLog(s, false);
	refreshMounts(s);
}

static void
open_activated 
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = (FSEDIT*)state;
	GtkWidget* opendialog = gtk_file_chooser_dialog_new
	(
		_("Open Mount File"),
		GTK_WINDOW(GETO("dfsedit")),
		GTK_FILE_CHOOSER_ACTION_OPEN,
		"_Cancel",
		GTK_RESPONSE_CANCEL,
		"_Open",
		GTK_RESPONSE_ACCEPT,
		NULL
	);
	
	if(gtk_dialog_run(GTK_DIALOG(opendialog)) == GTK_RESPONSE_ACCEPT)
	{
		GtkFileChooser* chooser = GTK_FILE_CHOOSER(opendialog);
		GFile* file = gtk_file_chooser_get_file(chooser);
		g_application_open(s->app, &file, 1, "");
	}
	
	gtk_widget_destroy(opendialog);
}

static void
opensys_activated
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = (FSEDIT*)state;
	GFile* file = g_file_new_for_path("/etc/fstab");
	g_application_open(s->app, &file, 1, "");
}

static void
saveas_activated
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = (FSEDIT*)state;
	GtkWidget* savedialog = gtk_file_chooser_dialog_new
	(
		_("Save Mount File"),
		GTK_WINDOW(GETO("dfsedit")),
		GTK_FILE_CHOOSER_ACTION_SAVE,
		"_Cancel",
		GTK_RESPONSE_CANCEL,
		"_Save",
		GTK_RESPONSE_ACCEPT,
		NULL
	);
	
	if(gtk_dialog_run(GTK_DIALOG(savedialog)) == GTK_RESPONSE_ACCEPT)
	{
		GtkFileChooser* chooser = GTK_FILE_CHOOSER(savedialog);
		GFile* file = gtk_file_chooser_get_file(chooser);
		if(do_save(s, g_file_get_path(file), _("Could not save %s")))
		{
			s->currentFile = file;
			dfsedit_updatetitle(s);
		}
	}
	
	gtk_widget_destroy(savedialog);
}

static void
save_activated 
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = (FSEDIT*)state;
	if(!s->currentFile)
	{
		saveas_activated(action, parameter, state);
	}
	
	if(s->currentFile)
	{
		do_save(s, g_file_get_path(s->currentFile), _("Could not save %s"));
	}
}

static void
backup_activated
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = (FSEDIT*)state;
	if(s->currentFile)
	{
		char backfile[1024];
		snprintf(backfile, 1024, "%s.bak", g_file_get_path(s->currentFile));
		do_save(s, backfile, _("Could not save backup file %s"));
	}
}

static void
mount_add_activated
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = (FSEDIT*)state;
	GtkDialog* dialog = dadd_reset(s, NULL, NULL, 0);
	if(gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_APPLY)
	{
		refreshMounts(s);
	}
	
	gtk_widget_hide(GTK_WIDGET(dialog));
}

static void
mount_edit_activated
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = state;
	MOUNTDEF* definition = getDefinitionFromSelected(s);
	if(definition)
	{
		GtkDialog* dialog = dadd_reset(s, &definition->entry, definition, definition->preset);
		if(gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_APPLY)
		{		
			refreshMounts(s);
		}
		
		gtk_widget_hide(GTK_WIDGET(dialog));
	}
}

static void
mount_remove_activated
	(GSimpleAction *action,
	 GVariant      *parameter,
	 gpointer       state)
{
	FSEDIT* s = state;
	mount_rm(&s->fsentries, getDefinitionFromSelected(s));
	refreshMounts(s);
}

static GActionEntry fsedit_win_actions[] =
{
	{ "new", new_activated, NULL, NULL, NULL },
	{ "open", open_activated, NULL, NULL, NULL },
	{ "opensys", opensys_activated, NULL, NULL, NULL },
	{ "save", save_activated, NULL, NULL, NULL },
	{ "saveas", saveas_activated, NULL, NULL, NULL },
	{ "backup", backup_activated, NULL, NULL, NULL },
	{ "mount.add", mount_add_activated, NULL, NULL, NULL },
	{ "mount.edit", mount_edit_activated, NULL, NULL, NULL },
	{ "mount.remove", mount_remove_activated, NULL, NULL, NULL },
};

void mapMenus(FSEDIT* s, GtkApplication* app, GtkApplicationWindow* window)
{
	g_action_map_add_action_entries (G_ACTION_MAP (app),
		fsedit_app_actions, G_N_ELEMENTS (fsedit_app_actions),
		s);
	
	g_action_map_add_action_entries(G_ACTION_MAP(window),
		fsedit_win_actions, G_N_ELEMENTS (fsedit_win_actions),
		s);
}