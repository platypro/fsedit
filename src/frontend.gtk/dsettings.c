//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma GCC diagnostic ignored "-Wunused-function"

#include "General.h"
#include "dsettings.h"

GObject* dsettings_init(FSEDIT* s)
{
	gtk_builder_add_from_resource(s->builder, "/net/platypro/fsedit/dsettings.ui", NULL);
	
	g_settings_bind (s->settings, SETTING_USE_CREDENTIALS,
			   GETO("dsettings_use_credentials"), "active",
			   G_SETTINGS_BIND_DEFAULT);
			   
	g_settings_bind (s->settings, SETTING_MOUNT_LOCATION,
			   GETO("dsettings_mount_location_entry"), "text",
			   G_SETTINGS_BIND_DEFAULT);
	
	g_settings_bind (s->settings, SETTING_PRIVATE_LOCATION,
			   GETO("dsettings_private_location_entry"), "text",
			   G_SETTINGS_BIND_DEFAULT);
	
	return GETO("dsettings");
}

//////////////////
// Start Events //
//////////////////

void
dsettings_close_clicked_cb
	(GtkButton *button,
     FSEDIT    *s)
{
	gtk_dialog_response (GTK_DIALOG(GETO("dsettings")),
		GTK_RESPONSE_CLOSE);
}

void
dsettings_reset_clicked_cb
	(GtkButton *button,
     FSEDIT    *s)
{
	g_settings_reset(s->settings, SETTING_USE_CREDENTIALS);
	g_settings_reset(s->settings, SETTING_MOUNT_LOCATION);
	g_settings_reset(s->settings, SETTING_PRIVATE_LOCATION);
}