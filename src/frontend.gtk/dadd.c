//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma GCC diagnostic ignored "-Wunused-function"

#include "General.h"
#include "dadd.h"

#include "backend.h"

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

PRESET_REF 
getActivePreset (FSEDIT* s)
{
	GtkComboBox* combo = GTK_COMBO_BOX(GETO("gadd_field_type"));
	return gtk_combo_box_get_active(combo);
}

GObject* dadd_init(FSEDIT* s)
{
	MOUNTPRESET* preset;
	int i;
	gtk_builder_add_from_resource(s->builder, "/net/platypro/fsedit/dadd.ui", NULL);
	
	s->dadd_fsname_indate = true;
	
	//Load mount type list
	GtkComboBoxText* combo = GTK_COMBO_BOX_TEXT(GETO("gadd_field_type"));
	
	mount_preset_iter(i, preset)
	{
		gtk_combo_box_text_append((GtkComboBoxText*)combo, NULL, preset->name);
	}

	gtk_combo_box_set_active(GTK_COMBO_BOX(combo), 1);
	
	return GETO("dadd");
}

void dadd_destroy(FSEDIT* s)
{
	gtk_widget_destroy(GTK_WIDGET(GETO("dadd")));
	return;
}

void
update_device_name
	(FSEDIT* s, 
	 const char* source)
{
	//update dadd_fsname_indate
	GtkEntry* fsentry = GTK_ENTRY(GETO("fmount_field_target"));
	
	static char newMountName[MOUNT_FILE_MAX];
	
	mount_preset_predict(
		getActivePreset(s),
		gtk_entry_get_text(fsentry),
		newMountName
	);
	
	s->dadd_fsname_indate = !strcmp(newMountName, source);
}

void 
update_file_name
	(FSEDIT* s, 
	 const char* source)
{
	GtkEntry* nameentry = GTK_ENTRY(GETO("fmount_field_mountname"));
	
	static char newMountName[MOUNT_FILE_MAX];
	
	if(s->dadd_fsname_indate)
	{
		mount_preset_predict(
				getActivePreset(s),
				source,
				newMountName
			);
		gtk_entry_set_text(nameentry, newMountName);
	}
}

//Used only inside dadd_setfields
static inline bool showFieldGroup(FSEDIT* s, MOUNTPRESET* preset, uint8_t mask, char* groupname)
{
	if(preset->fields & mask)
	{
		gtk_widget_show(GTK_WIDGET(GETO(groupname)));
		return true;
	}
	else
	{
		gtk_widget_hide(GTK_WIDGET(GETO(groupname)));
		return false;
	}
}

void dadd_setfields(FSEDIT* s, PRESET_REF view)
{
	MOUNTPRESET* preset = mount_preset_get(view);
	
	showFieldGroup(s, preset, MFIELD_LOCALPERMS, "fperms");
	showFieldGroup(s, preset, MFIELD_AUTH, "fauth");
	
	if(preset->fields & MFIELD_FORCEADVANCED)
		gtk_expander_set_expanded(GTK_EXPANDER(GETO("fadvanced")), 1);
	else
		gtk_expander_set_expanded(GTK_EXPANDER(GETO("fadvanced")), 0);
	
	gtk_entry_set_text(GTK_ENTRY(GETO("fadvanced_field_type")), *preset->type);
	gtk_entry_set_text(GTK_ENTRY(GETO("fmount_field_target")), preset->hint);
	gtk_entry_set_text(GTK_ENTRY(GETO("fadvanced_field_extra")), preset->defaults);
	
	s->dadd_fsname_indate = true;
	
	update_file_name(s, gtk_entry_buffer_get_text(GTK_ENTRY_BUFFER(GETO("fmount_field_target_buffer"))));
		
	return;
}

GtkDialog* dadd_reset(FSEDIT* s, ADDENTRY* customEntry, MOUNTDEF* mnt, PRESET_REF preset)
{
	ADDENTRY defaultEntry =
	{
		.perms  = ADDENTRY_PERMS_WRITE | ADDENTRY_PERMS_READ | ADDENTRY_PERMS_ALLUSERS | ADDENTRY_PERMS_NOLOGIN,
		
		.device = NULL,
		.file   = NULL,
		.type   = NULL,
		.opts   = "",
		.dump   = 0,
		.pass   = 2,
		
		.luser  = getlogin(),
		.lgroup = getlogin(),
		
		.ruser  = getlogin(),
		.rpass  = "",
	};
	
	const ADDENTRY* entry = customEntry ? customEntry : &defaultEntry;
	
	s->dadd_current_mnt = mnt;
	
	gtk_combo_box_set_active(GTK_COMBO_BOX(GETO("gadd_field_type")), preset);
	
	dadd_setfields(s, preset);
	
	//Reset fields
	gtk_toggle_button_set_active(
		GTK_TOGGLE_BUTTON(GETO("fperms_field_allowread")) , 
		entry->perms & ADDENTRY_PERMS_READ);
		
	gtk_toggle_button_set_active(
		GTK_TOGGLE_BUTTON(GETO("fperms_field_allowwrite")), 
		entry->perms & ADDENTRY_PERMS_WRITE);
		
	gtk_toggle_button_set_active(
		GTK_TOGGLE_BUTTON(GETO("fperms_field_allusers")), 
		entry->perms & ADDENTRY_PERMS_ALLUSERS);
	
	gtk_toggle_button_set_active(
		GTK_TOGGLE_BUTTON(GETO("fauth_field_guest")), 
		entry->perms & ADDENTRY_PERMS_NOLOGIN);
	
	gtk_widget_set_sensitive(GTK_WIDGET(GETO("fperms_account")), 
		!(entry->perms & ADDENTRY_PERMS_ALLUSERS));
	gtk_widget_set_sensitive(GTK_WIDGET(GETO("fauth_login")), 
		!(entry->perms & ADDENTRY_PERMS_NOLOGIN));

	if(entry->device)
		gtk_entry_set_text(GTK_ENTRY(GETO("fmount_field_target")), entry->device);
	if(entry->file)
		gtk_entry_set_text(GTK_ENTRY(GETO("fmount_field_mountname")), entry->file);
	if(entry->type)
		gtk_entry_set_text(GTK_ENTRY(GETO("fadvanced_field_type")), entry->type);
		
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(GETO("fadvanced_field_checkorder")), entry->pass);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(GETO("fadvanced_field_dump")), entry->dump);

	gtk_entry_set_text(
		GTK_ENTRY(GETO("fperms_field_localuser")), 
		entry->luser ? entry->luser : defaultEntry.luser);

	gtk_entry_set_text(
		GTK_ENTRY(GETO("fperms_field_localgroup")), 
		entry->lgroup ? entry->lgroup : defaultEntry.lgroup);
		
	gtk_entry_set_text(
		GTK_ENTRY(GETO("fauth_field_login")), 
		entry->ruser ? entry->ruser : defaultEntry.ruser);
		
	gtk_entry_set_text(
		GTK_ENTRY(GETO("fauth_field_password")), 
		entry->rpass ? entry->rpass : defaultEntry.rpass);
		
	if(entry->opts)
	gtk_entry_set_text(
		GTK_ENTRY(GETO("fadvanced_field_extra")), 
		entry->opts ? entry->opts : defaultEntry.opts);

	return GTK_DIALOG(GETO("dadd"));
}

bool dadd_apply(FSEDIT* s)
{
	if(!s->dadd_current_mnt)
	{
		//Create new mount
		MOUNTDEF* newdef;
		mount_add(&s->fsentries, &newdef);
		s->dadd_current_mnt = newdef;
	}
	
	MOUNTDEF* mnt = s->dadd_current_mnt;

	//Generate Options
	if(mnt)
	{
		ADDENTRY ae;
		
		//Prepare addentry
		
		ae.perms = 0;
		
		if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GETO("fperms_field_allowwrite"))))
			ae.perms |= ADDENTRY_PERMS_WRITE;
		
		if(!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GETO("fperms_field_allusers"))))
		{
			if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GETO("fperms_field_allowread"))))
				ae.perms |= ADDENTRY_PERMS_READ;
			ae.luser  = g_strdup(gtk_entry_get_text(GTK_ENTRY(GETO("fperms_field_localuser"))));
			ae.lgroup = g_strdup(gtk_entry_get_text(GTK_ENTRY(GETO("fperms_field_localgroup"))));
		} 
		else {ae.perms |= ADDENTRY_PERMS_ALLUSERS; ae.luser = NULL; ae.lgroup = NULL;ae.perms |= ADDENTRY_PERMS_READ;}
		
		if(!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GETO("fauth_field_guest"))))
		{
			ae.ruser = g_strdup(gtk_entry_get_text(GTK_ENTRY(GETO("fauth_field_login"))));
			ae.rpass = g_strdup(gtk_entry_get_text(GTK_ENTRY(GETO("fauth_field_password"))));
		}
		else {ae.perms |= ADDENTRY_PERMS_NOLOGIN; ae.ruser = NULL; ae.rpass = NULL;}
		
		ae.opts    = g_strdup(gtk_entry_get_text(GTK_ENTRY(GETO("fadvanced_field_extra"))));
		ae.device  = g_strdup(gtk_entry_get_text(GTK_ENTRY(GETO("fmount_field_target"))));
		ae.file    = g_strdup(gtk_entry_get_text(GTK_ENTRY(GETO("fmount_field_mountname"))));
		ae.type    = g_strdup(gtk_entry_get_text(GTK_ENTRY(GETO("fadvanced_field_type"))));
		ae.pass    = gtk_spin_button_get_value(GTK_SPIN_BUTTON(GETO("fadvanced_field_checkorder")));
		ae.dump    = gtk_spin_button_get_value(GTK_SPIN_BUTTON(GETO("fadvanced_field_dump")));

		mnt->entry = ae;
		mnt->preset = getActivePreset(s);
		
		return true;
	} 
	else return false;
}

//////////////////
// Start Events //
//////////////////

void
gadd_field_type_changed_cb 
	(GtkComboBox *widget,
     FSEDIT      *s)
{
	dadd_setfields(s, getActivePreset(s));
}

void
fperms_field_allusers_toggled_cb
	(GtkCheckButton *widget,
	 FSEDIT         *s)
{
	gtk_widget_set_sensitive(
		GTK_WIDGET(GETO("fperms_account")), 
		!gtk_toggle_button_get_active(
			GTK_TOGGLE_BUTTON(
				GETO("fperms_field_allusers")
			)
		)
	);
}

void
fmount_field_mountname_buffer_inserted_text_cb
	(GtkEntryBuffer *buffer,
	 guint           position,
	 gchar          *chars,
	 guint           n_chars,
	 FSEDIT         *s)
{
	update_device_name(s, gtk_entry_buffer_get_text(buffer));
}
 
void
fmount_field_mountname_buffer_deleted_text_cb
	(GtkEntryBuffer *buffer,
	 guint           position,
	 guint           n_chars,
	 FSEDIT         *s)
{
	update_device_name(s, gtk_entry_buffer_get_text(buffer));
}

void
fauth_field_guest_toggled_cb
	(GtkCheckButton *widget,
	 FSEDIT         *s)
{
	gtk_widget_set_sensitive(
		GTK_WIDGET(GETO("fauth_login")), 
		!gtk_toggle_button_get_active(
			GTK_TOGGLE_BUTTON(
				GETO("fauth_field_guest")
			)
		)
	);
}

void
fmount_field_target_buffer_insert_text_cb
	(GtkEntryBuffer *buffer,
     guint           position,
     gchar          *chars,
     guint           n_chars,
     FSEDIT         *s)
{
	update_file_name(s, gtk_entry_buffer_get_text(buffer));
}
 
void
fmount_field_target_buffer_deleted_text_cb
	(GtkEntryBuffer *buffer,
	 guint           position,
	 guint           n_chars,
	 FSEDIT         *s)
{
	update_file_name(s, gtk_entry_buffer_get_text(buffer));
}

void 
dadd_cancel_clicked_cb
	(GtkButton *button,
     FSEDIT         *s)
{
	gtk_dialog_response (GTK_DIALOG(GETO("dadd")), 
		GTK_RESPONSE_CANCEL);
}

void
dadd_add_clicked_cb
	(GtkButton *button,
   FSEDIT    *s)
{
	gtk_dialog_response (GTK_DIALOG(GETO("dadd")), 
		dadd_apply(s) ? GTK_RESPONSE_APPLY : GTK_RESPONSE_CANCEL);
}


void
dadd_key_press_event_cb
	(GtkWidget *widget,
	 GdkEvent  *event,
	 FSEDIT    *s)
{
	guint key;
	gdk_event_get_keyval(event, &key);
	if(key == GDK_KEY_Return)
	{
		dadd_add_clicked_cb(NULL, s);
	}
	else if(key == GDK_KEY_Escape)
	{
		dadd_cancel_clicked_cb(NULL, s);
	}
}