//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "General.h"
#include "config.h"
#include "fsedit.gtk.h"

#include "backend.h"
#include "dadd.h"
#include "dfsedit.h"
#include "menus.h"
#include "dsettings.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>

void fsedit_ui_init(GtkApplication* app, FSEDIT* s)
{
	//Load Static UI Data
	s->builder = gtk_builder_new();
	s->app = G_APPLICATION(app);
	s->settings = g_settings_new("net.platypro.fsedit");
	
	//Set up windows
	GObject* dadd    = dadd_init(s);
	GObject* dfsedit = dfsedit_init(s);
	GObject* dsettings = dsettings_init(s);
	
	gtk_window_set_icon_from_file(GTK_WINDOW(dfsedit), APP_ICON_PATH, NULL);
	printf("Icon path:%s\n", APP_ICON_PATH);
	
	gtk_application_add_window(app, GTK_WINDOW(dfsedit));
	
	mapMenus(s, app, GTK_APPLICATION_WINDOW(dfsedit));
	
	//Connect Event Handlers
	gtk_builder_connect_signals (s->builder, s);
	
	if(dfsedit && dadd && dsettings)
	{
		//Set parenting
		gtk_window_set_transient_for(GTK_WINDOW(dadd), GTK_WINDOW(dfsedit));
		//Show window
		gtk_widget_show(GTK_WIDGET(dfsedit));
	}
}

void 
fsedit_activate
	(GApplication* application, 
	 FSEDIT* s)
{
	s->currentFile = g_file_new_for_path ("/etc/fstab");
	g_application_open(application, &s->currentFile, 1, NULL);
}

void fsedit_startup
	(GtkApplication *application,
	 FSEDIT         *s)
{
	fsedit_ui_init(application, s);
}

void 
fsedit_open
	(GtkApplication  *application,
	 GFile        **files,
	 gint           n_files,
	 gchar         *hint,
	 FSEDIT        *s)
{
	mount_clear(&s->fsentries);
	g_object_ref(*files);
	if(s->currentFile)
		g_object_unref(s->currentFile);
	UPDATE_OPTS(s);
	s->currentFile = *files;
	dfsedit_showLog(s, false);
	dfsedit_clearLog(s);
	if(mount_load(&s->fsentries, g_file_get_path(s->currentFile), &s->opts))
	{
		s->currentFile = NULL;
		dfsedit_showLog(s, true);
	}
	refreshMounts(s);
	dfsedit_updatetitle(s);
}

void fsedit_shutdown
	(GtkApplication *app,
	 FSEDIT         *s)
{
	dadd_destroy(s);
	dfsedit_destroy(s);
	mount_clear(&s->fsentries);
	g_object_unref(s->builder);
	free(s);
}

static inline void pushErrorText(GtkTextBuffer* buffer, GtkTextMark* mark, char* contents, char* tag)
{
	GtkTextIter iter;
	gtk_text_buffer_get_iter_at_mark(buffer, &iter, mark);
	gtk_text_buffer_insert_with_tags_by_name(
		buffer,
		&iter,
		contents,
		-1,
		tag,
		NULL);
}

void fsedit_error (void* state, MOUNT_ERRCODE errcode, char* device, char* message)
{
	FSEDIT* s = (FSEDIT*)state;
	char* error = NULL;
	switch(errcode)
	{
		case MOUNT_ERR_PERMS:
			error = _(" can not be accessed due to a permission error");
			break;
		case MOUNT_ERR_BADUSER:
			error = _(" is not a valid user");
			break;
		case MOUNT_ERR_BADGROUP:
			error = _(" is not a valid group");
			break;
		default: return;
	}

	GtkTextMark* mark;
	GtkTextIter iter;
	GtkTextBuffer* buffer = GTK_TEXT_BUFFER(GETO("dfsedit_log_buffer"));

	fprintf(stderr, "%s: %s%s\n", device, message ? message : "", error);
	gtk_text_buffer_get_end_iter(buffer, &iter);
	mark = gtk_text_buffer_create_mark(buffer, NULL, &iter, FALSE);
		
	pushErrorText(buffer, mark, device, BUFFER_TAG_ERR_CONTENTS);
	pushErrorText(buffer, mark, ": ", BUFFER_TAG_ERR_CONTENTS);
	if(message)
		pushErrorText(buffer, mark, message, BUFFER_TAG_ERR_EXTRA);
	pushErrorText(buffer, mark, error, BUFFER_TAG_ERR_CONTENTS);	
	pushErrorText(buffer, mark, "\n", BUFFER_TAG_ERR_CONTENTS);
}

int main(int argc, char** argv)
{
	FSEDIT* s = malloc(sizeof(FSEDIT));

	if(s)
	{
		GtkApplication* fsedit;
		
		s->opts = (IO_OPTS){
			.useCredentials = false,
			.privatePath    = NULL,
			.mountPath      = NULL,
			.error          = fsedit_error,
			.state          = s };
		
		//Set Up GUI
		fsedit = gtk_application_new("net.platypro.fsedit", G_APPLICATION_HANDLES_OPEN);
		g_signal_connect(fsedit, "startup" , G_CALLBACK(fsedit_startup), s);
		g_signal_connect(fsedit, "open"    , G_CALLBACK(fsedit_open), s);
		g_signal_connect(fsedit, "activate", G_CALLBACK(fsedit_activate), s);
		g_signal_connect(fsedit, "shutdown", G_CALLBACK(fsedit_shutdown), s);
		g_application_run(G_APPLICATION(fsedit), argc, argv);
		g_object_unref(fsedit);
	}

	return 1;
}
