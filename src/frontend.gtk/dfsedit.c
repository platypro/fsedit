//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma GCC diagnostic ignored "-Wunused-function"

#include "General.h"
#include "dfsedit.h"

#include "backend.h"
#include "dadd.h"

#include <gtk/gtk.h>
#include <glib/gi18n.h>

void dfsedit_updatetitle(FSEDIT* s)
{
	char* title = s->currentFile
	 ? g_strdup_printf("fsedit - %s", g_file_get_path(s->currentFile))
	 : "fsedit - No File";

	gtk_window_set_title(
		GTK_WINDOW(GETO("dfsedit")),
		title);
}

void refreshMounts(FSEDIT* s)
{
	//Load partition list
	GtkListStore* partlist = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(GETO("dfsedit_mounts"))));

	//Clear list
	gtk_list_store_clear(partlist);
	
	GtkTreeIter treeiter;
	
	//Load list items
	MOUNTDEF* defat;
	mount_iter(&s->fsentries, defat)
	{
		gtk_list_store_append(partlist, &treeiter);
		gtk_list_store_set(
			partlist, &treeiter,
			0, defat->entry.device,
			1, defat->entry.file,
			2, defat->entry.type,
			3, defat->entry.opts,
			4, defat,
			-1);
	}
}

void addcol(GtkTreeView* view, GtkCellRenderer* renderer, char* text, int coldata)
{
	GtkTreeViewColumn* newcol;
	newcol = gtk_tree_view_column_new_with_attributes
	(
		text,
		renderer,
		"text", coldata,
		NULL
	);
	gtk_tree_view_append_column(view, newcol);
}

GObject* dfsedit_init(FSEDIT* s)
{
	gtk_builder_add_from_resource(s->builder, "/net/platypro/fsedit/dfsedit.ui", NULL);
	
	GtkCellRenderer* textrender = gtk_cell_renderer_text_new();
	GtkTreeView* treeview = GTK_TREE_VIEW(GETO("dfsedit_mounts")); 
	GtkListStore* listmodel = gtk_list_store_new(5, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_POINTER);
	gtk_tree_view_set_model(treeview, GTK_TREE_MODEL(listmodel));
	
	addcol(treeview, textrender, _("Mount Location"), 0);
	addcol(treeview, textrender, _("Mount Name"), 1);
	addcol(treeview, textrender, _("Filesystem Type"), 2);
	addcol(treeview, textrender, _("Options"), 3);
	
	//Set log tags
	GtkTextBuffer* logbuffer = GTK_TEXT_BUFFER(GETO("dfsedit_log_buffer"));
	gtk_text_buffer_create_tag (logbuffer, BUFFER_TAG_ERR_CONTENTS,
	   		            "foreground", "#cc0000", NULL);
	gtk_text_buffer_create_tag (logbuffer, BUFFER_TAG_ERR_EXTRA,
	   		            "foreground", "#ff6600", NULL);  
	
	return GETO("dfsedit");
}

void dfsedit_destroy(FSEDIT* s)
{
	gtk_widget_destroy(GTK_WIDGET(GETO("dfsedit")));
	return;
}

void dfsedit_showLog(FSEDIT* s, bool shown)
{
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(GETO("dfsedit_menu_showlog")), shown);
}

void dfsedit_clearLog(FSEDIT* s)
{
	gtk_text_buffer_set_text(GTK_TEXT_BUFFER(GETO("dfsedit_log_buffer")), "", 0);
}

//////////////////
// Start Events //
//////////////////

void dfsedit_action_add_clicked_cb
	(GtkButton *button,
     FSEDIT    *s)
{	
	g_action_activate(g_action_map_lookup_action(G_ACTION_MAP(GETO("dfsedit")), "mount.add"), NULL);
}

void
dfsedit_action_edit_clicked_cb
	(GtkButton *button,
     FSEDIT    *s)
{
	g_action_activate(g_action_map_lookup_action(G_ACTION_MAP(GETO("dfsedit")), "mount.edit"), NULL);
}

void
dfsedit_action_remove_clicked_cb
	(GtkButton *button,
     FSEDIT    *s)
{
	g_action_activate(g_action_map_lookup_action(G_ACTION_MAP(GETO("dfsedit")), "mount.remove"), NULL);
}

void
dfsedit_menu_showlog_toggled_cb
	(GtkCheckMenuItem *checkmenuitem,
     FSEDIT           *s)
{
	gboolean isShown = gtk_check_menu_item_get_active(checkmenuitem);
	gtk_revealer_set_reveal_child(GTK_REVEALER(GETO("dfsedit_log_revealer")), isShown);
}

void
dfsedit_log_hide_clicked_cb
	(GtkButton *button,
     FSEDIT    *s)
{
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(GETO("dfsedit_menu_showlog")), false);
}

void
dfsedit_log_clear_clicked_cb
	(GtkButton *button,
     FSEDIT    *s)
{
	dfsedit_clearLog(s);
}