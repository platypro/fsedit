//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef INC_FSEDIT_H_
#define INC_FSEDIT_H_

#include <stdint.h>
#include <gtk/gtk.h>

#include "backend.h"

#define SETTING_USE_CREDENTIALS  "use-credentials"
#define SETTING_MOUNT_LOCATION   "mount-location"
#define SETTING_PRIVATE_LOCATION "private-location"

#define BUFFER_TAG_ERR_CONTENTS "err_contents"
#define BUFFER_TAG_ERR_EXTRA "err_extra"

#define GETO(thing) gtk_builder_get_object(s->builder, (thing))

extern void fsedit_error (void* user, MOUNT_ERRCODE errcode, char* device, char* message);

#define UPDATE_OPTS(s) \
	s->opts.useCredentials = g_settings_get_boolean((s)->settings, SETTING_USE_CREDENTIALS); \
	s->opts.privatePath    = g_settings_get_string((s)->settings, SETTING_PRIVATE_LOCATION); \
	s->opts.mountPath      = g_settings_get_string((s)->settings, SETTING_MOUNT_LOCATION);

typedef struct FsEdit
{
	GtkBuilder* builder;
	GSettings* settings;
	GApplication* app;
	GFile* currentFile; // Current loaded file
	
	IO_OPTS opts;
	MOUNTHEAD fsentries;
	
	//Dadd properties
	MOUNTDEF* dadd_current_mnt;
	uint8_t dadd_fsname_indate;

} FSEDIT;

#endif