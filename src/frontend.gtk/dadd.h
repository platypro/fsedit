//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "fsedit.gtk.h"

#include <gtk/gtk.h>

#define FSCACHE_AUTONAME 1

struct AddEntry;
struct MountDef;

extern GObject* dadd_init(FSEDIT*);
extern GtkDialog* dadd_reset(FSEDIT*, struct AddEntry*, struct MountDef*, uint8_t);
extern void dadd_destroy(FSEDIT*);