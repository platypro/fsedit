//fsedit - A modern and intuitive GUI for managing mounts
//Copyright (c) 2017 Aeden McClain
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "fsedit.gtk.h"

#include <gtk/gtk.h>

extern GObject* dfsedit_init(FSEDIT*);
extern void dfsedit_destroy(FSEDIT*);

extern void dfsedit_updatetitle(FSEDIT* s);
extern void dfsedit_showLog(FSEDIT* s, bool shown);
extern void dfsedit_clearLog(FSEDIT* s);
extern void refreshMounts(FSEDIT* s); //< Refresh mount list