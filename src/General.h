//General Headers
//These are pretty basic

#ifndef INC_GENERAL_H_
#define INC_GENERAL_H_

#include <stdint.h>

//Definition for bool value
typedef uint8_t bool;
//True and False Definitions
#define false 0
#define true !false //!< It's funny because it's true

//Public and Private Definitions
#define PUBLIC
#define PRIVATE static

#define NUMELEMENTS(array) sizeof(array) / sizeof(*array)

#ifndef NULL
#define NULL 0
#endif

#endif
